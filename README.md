# DotNetToJScript

Binary build of https://github.com/tyranid/DotNetToJScript/


## Builds

https://gitlab.com/pgregoire-ci/dotnettojscript/-/jobs/artifacts/main/download?job=build


## Usage

```
# setup wine environment
sudo apt-get -y install wine32 winetricks
WINEARCH=win32
WINEPREFIX="${HOME}/.wine/dotnettojscript"
mkdir -p "${WINEPREFIX}"
export WINEARCH WINEPREFIX
wineboot --init
winetricks winxp
winetricks dotnet35sp1

# run
wine ./DotNetToJScript.exe
```
